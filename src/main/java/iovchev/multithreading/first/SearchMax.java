package iovchev.multithreading.first;

public class SearchMax extends Thread {
    private int[] array;
    private Integer max = null;

    public SearchMax(int[] array) {
        this.array = array;
    }

    @Override
    public void run() {
        max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }

    }

    public Integer getMax() {

        while (max == null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return max;
    }

}
