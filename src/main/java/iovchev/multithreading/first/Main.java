package iovchev.multithreading.first;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

//        Пользователь вводит с клавиатуры значение в массив.
//        После чего запускаются два потока.
//        Первый поток находит максимум в массиве, второй — минимум.
//        Результаты вычислений возвращаются в метод main().
public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            String number;
            while (true) {
                number = reader.readLine();
                if (!number.equals("")) {
                    break;
                }
            }
            array[i] = Integer.valueOf(number);
        }

        SearchMax searchMax = new SearchMax(array);
        searchMax.start();
        int max = searchMax.getMax();

        SearchMin searchMin = new SearchMin(array);
        searchMin.start();
        int min = searchMin.getMin();

        System.out.println("Array: " + Arrays.toString(array));
        System.out.println("The maximum is: " + max);
        System.out.println("The minimum is: " + min);
    }
}
