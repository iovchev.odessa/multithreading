package iovchev.multithreading.first;

public class SearchMin extends Thread {
    private int[] array;
    private Integer min = null;

    public SearchMin(int[] array) {
        this.array = array;
    }

    @Override
    public void run() {
        min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
    }

    public Integer getMin() {
        while (min == null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return min;
    }
}
