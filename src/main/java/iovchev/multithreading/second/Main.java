package iovchev.multithreading.second;

//        Заданы три целочисленных массива.
//        Записать эти массивы в файл в паралельних потоках.
//        Создать класс SaveAsThread для представления потока, который записывает целочисленный массив в файл.

import java.io.IOException;
import java.io.RandomAccessFile;

public class Main {
    public static void main(String[] args) throws IOException {
        int[] arrayFirst = {1, 1, 1, 1, 1, 1, 1};
        int[] arraySecond = {2, 2, 2, 2, 2, 2, 2};
        int[] arrayThird = {3, 3, 3, 3, 3, 3, 3};

        String fileName = "FileMultithreading.txt";
        RandomAccessFile randomAccessFile = new RandomAccessFile(fileName, "rw");
        randomAccessFile.setLength(0);

        SaveAsThread saveAsThreadFirst = new SaveAsThread(arrayFirst, randomAccessFile);
        saveAsThreadFirst.start();

        SaveAsThread saveAsThreadSecond = new SaveAsThread(arraySecond, randomAccessFile);
        saveAsThreadSecond.start();

        SaveAsThread saveAsThreadThird = new SaveAsThread(arrayThird, randomAccessFile);
        saveAsThreadThird.start();
    }
}
