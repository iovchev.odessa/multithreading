package iovchev.multithreading.second;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

public class SaveAsThread extends Thread {
    private int[] array;
    private RandomAccessFile randomAccessFile;

    public SaveAsThread(int[] array, RandomAccessFile randomAccessFile) {
        this.array = array;
        this.randomAccessFile = randomAccessFile;
    }

    @Override
    public void run() {
        try {
            randomAccessFile.seek(randomAccessFile.length());
            randomAccessFile.write((Arrays.toString(array) + "\n").getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
